# Idempotent Data Spike

The spike in which we make the test data idempotent.

Requires

- poetry
- docker

## usage

```shell
poetry install
poetry run robot dockering.robot
```

## Notes

- `postgres_loaded.yaml` docker-compose definition which calls the SQL files in
the `sql` folder.
- `postgres_loaded.cfg` database config used by the database library in robot
- `dockering.robot` test cases which runs the query tests
- `dockering.resource` Robot Resource file which does the heavy lifting

The official postgres docker image will run `.sql` scripts found in the 
`/docker-entrypoint-initdb.d/` folder.


