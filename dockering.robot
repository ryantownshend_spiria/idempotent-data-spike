*** Settings ***
Documentation               Spike to figure out idempotent PostgreSQL data management
Library                     Process
Library                     DatabaseLibrary
Resource                    dockering.resource
Suite Setup                 Orchestrate Setup
Suite Teardown              Orchestrate Teardown


*** Test Cases ***
First Test Case
    [Documentation]         Empty first test
    Log To Console          Empty First Test

Basic Query Test
    [Documentation]         Setup and tear down from setting section
    Log To Console          Basic Query Test
    ${rowCount}             Row Count   select * from city
    Log To Console          Row Count ${rowCount}
    Should Be Equal As Integers        ${rowCount}     30

Basic Insert Test
    [Documentation]         Setup and tear down from setting section
    Log To Console          Basic Insert Test
    ${output} =    Execute SQL String   INSERT INTO public.city(city_id, city_name, country_id) VALUES (31, 'newcity', 1);
    Log To Console   ${output}

Second Query Test
    [Documentation]         Setup and tear down from setting section
    Log To Console          Second Query Test
    ${rowCount}             Row Count   select * from city
    Log To Console          Row Count ${rowCount}
    Should Be Equal As Integers        ${rowCount}     31